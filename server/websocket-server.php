<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

require dirname(__DIR__) . '/vendor/autoload.php';
require dirname(__DIR__) . '/src/MyApp/Main.php';
require dirname(__DIR__) . '/src/MyApp/Product.php';

use MyApp\Main;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            new Main()
        )
    )
  , 8080 // Change port here
);

$server->run();
