<?php
namespace MyApp;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use MyApp\Product;

class Main implements MessageComponentInterface {

    protected $clients;
    protected $Product;

    public function __construct() {

        $this->clients = new \SplObjectStorage;

        if ( count($this->clients) <= 0 ) { echo "socket ready\n"; }
    }

    public function onOpen(ConnectionInterface $conn) {

        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
        
        $this->callProduct();
    }

    public function onMessage(ConnectionInterface $from, $cmd) {

        echo "Received a new command: $cmd\n";
        $decodeData = json_decode($cmd);

        if ( !empty($decodeData) ) {
            
            switch ($decodeData->msg) {
                case 'importProduct':
                    $this->Product->importProduct($decodeData->data);
                    $this->callProduct();
                    break;
                default:
                    # code...
                    break;
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {

        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {

        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }

    public function callProduct()
    {
        $this->Product = new Product;
        $datas = $this->Product->fetchAllProduct();

        foreach ($this->clients as $client) {

            $client->send(json_encode($datas));
        }
    }
}