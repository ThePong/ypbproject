<?php

namespace MyApp;
use Medoo\Medoo;

class Product {

    protected $database;

    public function __construct() {
        
        echo "Product init\n";
        $this->dbConnect();
    }

    public function dbConnect()
    {
        $this->database = new Medoo([
            'database_type' => 'mysql',
            'database_name' => 'ybp_project',
            'server' => 'localhost',
            'username' => 'root',
            'password' => ''
        ]);

        if (  $this->database->error() ) {
            
            echo "Database Disconnected\n";
        }
        else {

            echo "Database Connected\n";
        }
    }

    public function fetchAllProduct()
    {
        $datas = $this->database->select("product", [
            "id",
            "name",
            "description",
            "price",
            "stock",
        ]);

        return $datas;
    }

    public function importProduct($data)
    {
        $this->database->insert("product", [
            "name" => $data->name,
            "description" => $data->description,
            "price" => $data->price,
            "stock" => $data->stock,
        ]);
    }
}