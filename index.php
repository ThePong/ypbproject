<!DOCTYPE html>
<html>
<head>
	<title></title>

	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	<link href="https://bootswatch.com/4/cosmo/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
	<script
	  src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
	  integrity="sha256-3edrmyuQ0w65f8gfBsqowzjJe2iM6n0nKciPUp8y+7E="
	  crossorigin="anonymous"></script>

  	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


  <script type="text/javascript">
  	
  	var conn = null;
	var isConnected = false;
	var wsUri = 'ws://127.0.0.1:8080/'; // socket server destination

	$(function() {
		toggleConnect();
	});

	function setOnline() {
		$(".socketStatus").removeClass("badge-warning");
		$(".socketStatus").addClass("badge-success");
		$(".socketStatus").html("WebSocket Status : Connected");
		isConnected = true;
	}

	function setOffline() {
		$(".socketStatus").addClass("badge-warning");
		$(".socketStatus").removeClass("badge-success");
		$(".socketStatus").html("WebSocket Status : Disconnected");
		isConnected = false;
	}

	function toggleConnect() {

		if (isConnected) {
			setOffline();
			return;
		}

		conn = new WebSocket(wsUri);

		conn.onmessage = function(e) {

			console.info(e);
			if ( e.data ) {

				var data = $.parseJSON(e.data);
				console.log('data', data);
				addProductToTable(data);
			}
		}

		conn.onopen = function(e) {
			console.info(e);
			setOnline();
			console.info("Connected");
			isConnected = true;
		};

		conn.onclose = function(e) {
			console.info("Disconnected");
			setOffline();
		};

	}

	function addProductToTable(data) {

		for (var i = 0; i < data.length; i++) {

			if ( $(".productTable tr#"+data[i].id).length <= 0 ) { // Update New Data

				$('.productTable tbody').append(`
					<tr id="${data[i].id}">
				      <td class="name">${data[i].name}</td>
				      <td class="description">${data[i].description}</td>
				      <td class="price">${data[i].price}</td>
				      <td class="stock">${data[i].stock}</td>
				    </tr>
				`);
			}
			else { // Update Exist Data

				$('.productTable tbody tr#'+data[i].id+" .name").text(data[i].name);
				$('.productTable tbody tr#'+data[i].id+" .description").text(data[i].description);
				$('.productTable tbody tr#'+data[i].id+" .price").text(data[i].price);
				$('.productTable tbody tr#'+data[i].id+" .stock").text(data[i].stock);
			}
		}

		// $('.jsonData').text(JSON.stringify(data));
	}

	function importProduct() {
		
		var name = $('#modal-import #input-name').val();
		var description = $('#modal-import #input-description').val();
		var price = $('#modal-import #input-price').val();
		var stock = $('#modal-import #input-stock').val();

		if ( name && description && price && stock ) {

			var data = {
				name: name,
				description: description,
				price: price,
				stock: stock,
			};

			console.log('importProduct', data);

			var sendFormat = {

				msg: 'importProduct',
				data: data
			}

			conn.send(JSON.stringify(sendFormat));

			$('#modal-import').modal('hide');
		}
		else {

			alert('Please full fill import form.');
			return;
		}
	}

	// CSV Fn

	function downloadCSV(csv, filename) {
	    var csvFile;
	    var downloadLink;

	    // CSV file
	    csvFile = new Blob([csv], {type: "text/csv"});

	    // Download link
	    downloadLink = document.createElement("a");

	    // File name
	    downloadLink.download = filename;

	    // Create a link to the file
	    downloadLink.href = window.URL.createObjectURL(csvFile);

	    // Hide download link
	    downloadLink.style.display = "none";

	    // Add the link to DOM
	    document.body.appendChild(downloadLink);

	    // Click download link
	    downloadLink.click();
	}

	function exportTableToCSV(filename) {

		var delimited = prompt('Please specific delimited. (default is ",")', ',');
		if ( !delimited ) { delimited = ","; }

	    var csv = [];
	    var rows = document.querySelectorAll("table tr");
	    
	    for (var i = 0; i < rows.length; i++) {
	        var row = [], cols = rows[i].querySelectorAll("td, th");
	        
	        for (var j = 0; j < cols.length; j++) 
	            row.push(cols[j].innerText);
	        
	        csv.push(row.join(delimited));
	    }

	    // Download CSV file
	    downloadCSV(csv.join("\n"), filename);
	}

	// CSV Fn

  </script>

  <style type="text/css">

	body {

		margin: 0 auto;
		width: 60%;
	}
  	
  	.left {

  		text-align: left;
  	}

  	.right {

  		text-align: right;
  	}

  </style>

</head>
<body>

	<div class="row">
		
		<div class="col-md-12 right">
			<span class="badge badge-success socketStatus">WebSocket Status : Connected</span>
			<button class="btn btn-primary" data-toggle="modal" data-target="#modal-import">Import</button>
			<button class="btn btn-success" onclick="exportTableToCSV('members.csv')">Export To CSV</button>
		</div>

	</div>

	<div class="row">
		
		<div class="col-md-12">
			<table class="table table-hover productTable">
				<thead>
				    <tr>
				      <th scope="col">Name</th>
				      <th scope="col">Description</th>
				      <th scope="col">Price</th>
				      <th scope="col">Stock</th>
				    </tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>

	</div>

	<!-- Import Modal -->

	<div id="modal-import" class="modal">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Import Product</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">

	      	<div class="form-group">
		      <label for="input-name">Name</label>
		      <input class="form-control" type="text" name="input-name" id="input-name" placeholder="product name">
		    </div>
		    <div class="form-group">
		      <label for="input-description">Description</label>
		      <textarea class="form-control" name="input-description" id="input-description" placeholder="description"></textarea>
		    </div>
		    <div class="form-group">
		      <label for="input-price">Price</label>
		      <input class="form-control" type="number" name="input-price" id="input-price" placeholder="price">
		    </div>
		    <div class="form-group">
		      <label for="input-stock">Stock</label>
		      <input class="form-control" type="number" name="input-stock" id="input-stock" placeholder="stock">
		    </div>

	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" onclick="javascript:importProduct();">Import</button>
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Import Modal -->

</body>
</html>