# YPB assignment

## Install

First, run composer to install dependencies.

## Usage & Run

* For client side, file name is "index.php" (root of project folder)

When client ready for connect to socket, on top of page it would show "WebSocket Status : Connected"

* For server side, destination is "/server/websocket-server.php".

You have to run server by open command then "php server/websocket-server.php" or double click on file "run-server.sh" for auto execute.

When it ready you will see msg from server "socket ready".

### DB Config

Import table here "/ybp_project.sql"

Config env location "/src/MyApp/Product.php"

```
$this->database = new Medoo([
    'database_type' => 'mysql',
    'database_name' => 'your_database_name',
    'server' => 'localhost',
    'username' => 'your_username',
    'password' => 'your_password'
]);
```

### Note

Enable php_sockets extension. (php_sockets.dll)

Socket port 8080 (Please make sure port it free)

DB library https://medoo.in/

### My environment

PHP Version 5.6.33

Apache/2.4.29

mysqlnd 5.0.11-dev